package com.epam.orderservice.controllers;

import com.epam.orderservice.OrderService;
import com.epam.orderservice.models.Order;
import javax.validation.Valid;

import com.epam.orderservice.validators.OrderValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @Autowired
    Validator orderValidator;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.addValidators(orderValidator);
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Order> index() {
        return orderService.getOrders();
    }

    @PostMapping
    public Order create(@RequestBody @Valid Order order) {

        // Required by Hibernate ORM to save properly
        if (order.getItems() != null) {
            order.getItems().forEach(item -> item.setOrder(order));
        }
        return orderService.saveOrder(order);
    }

    @RequestMapping("/{id}")
    public Order view(@PathVariable("id") long id) {
        return orderService.getOrder(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public Order edit(@PathVariable("id") long id, @RequestBody @Valid Order order) {

        Order updatedOrder = orderService.getOrder(id);

        if (updatedOrder == null) {
            return null;
        }

        return orderService.saveOrder(order);
    }
}
