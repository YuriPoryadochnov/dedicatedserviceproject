package com.epam.orderservice.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class TokenAuthenticationFilter extends GenericFilterBean {

    @Value("${jwt.header}")
    private String AUTH_HEADER;

    @Value("${url.auth}")
    private String url;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        final String token = ((HttpServletRequest) request).getHeader(AUTH_HEADER);
        if (token == null) {
            chain.doFilter(request, response);
            return;
        }
        HttpHeaders headers = new HttpHeaders();
        headers.set(AUTH_HEADER, token);
        HttpEntity entity = new HttpEntity(headers);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<TokenBasedAuthentication> authResponse = restTemplate.exchange(
                url, HttpMethod.GET, entity, TokenBasedAuthentication.class);
        TokenBasedAuthentication tokenBasedAuthentication = authResponse.getBody();
        if(tokenBasedAuthentication == null){
            chain.doFilter(request, response);
        }
        SecurityContextHolder.getContext().setAuthentication(authResponse.getBody());
        chain.doFilter(request, response);
    }
}